#include "Object.h"
#include "Plane.h"
#include <vector>
#include "glm/glm.hpp"
#include "glm/mat4x4.hpp"
#include <glm/gtc/matrix_transform.hpp>

class Cube : public Object
{
protected:
	Vector centre;

	glm::mat4x4 rot;
	glm::mat4x4 invRot;
	glm::mat4x4 transRot;

	float rotation;

	Plane* top;
	Plane* bottom;

	Plane* left;
	Plane* right;

	Plane* back;
	Plane* front;

	std::vector<Plane*> sides;

	Vector transform(const glm::mat4x4 &t, Vector vec);
public:
	Cube(Vector centre, float width, float height, float length, float rotation, Color col)
		:rotation(rotation), centre(centre)
	{
		Vector halfSize = Vector(width, height, length) * 0.5f;
		float minX = -halfSize.x;
		float minY = -halfSize.y;
		float minZ = -halfSize.z;
		float maxX = halfSize.x;
		float maxY = halfSize.y;
		float maxZ = halfSize.z;

		color = col;

		rot = glm::mat4x4(1.0);

		Vector topLeftBack = transform(rot, Vector(minX, minY, minZ)) ;
		Vector topRightBack = transform(rot, Vector(maxX, minY, minZ)) ;
		Vector topRightFront = transform(rot, Vector(maxX, minY, maxZ)) ;
		Vector topLeftFront = transform(rot, Vector(minX, minY, maxZ)) ;

		Vector bottomLeftBack = transform(rot, Vector(minX, maxY, minZ)) ;
		Vector bottomRightBack = transform(rot, Vector(maxX, maxY, minZ)) ;
		Vector bottomRightFront = transform(rot, Vector(maxX, maxY, maxZ)) ;
		Vector bottomLeftFront = transform(rot, Vector(minX, maxY, maxZ)) ;

		top = new Plane(topLeftFront, topRightFront, topRightBack, topLeftBack, color);
		bottom = new Plane(bottomLeftBack, bottomRightBack, bottomRightFront, bottomLeftFront, color);

		left = new Plane(bottomLeftBack, bottomLeftFront, topLeftFront, topLeftBack, color);
		right = new Plane(topRightBack, topRightFront, bottomRightFront, bottomRightBack, color);

		back = new Plane(topLeftBack, topRightBack, bottomRightBack, bottomLeftBack, color);
		front = new Plane(bottomLeftFront, bottomRightFront, topRightFront, topLeftFront, color);

		sides.push_back(top);
		sides.push_back(bottom);
		sides.push_back(left);
		sides.push_back(right);
		sides.push_back(back);
		sides.push_back(front);

		rot = glm::rotate(rot, rotation, glm::vec3(0, 1, 0));
		invRot = glm::inverse(rot);
		transRot = glm::transpose(rot);
	}

	float intersect(Vector pos, Vector dir);

	Vector normal(Vector pos);
};