#ifndef H_PSPHERE
#define H_PSPHERE

#include "Sphere.h"

class PatternedSphere : public Sphere
{
private:
	int longditudinalSections, latitudinalSections;

	Color color2;

public:
	PatternedSphere(Vector centre, float radius, int horizontalSections, int verticalSections, Color col1, Color col2)
		:Sphere(centre, radius, col1)
	{
		longditudinalSections = horizontalSections;
		latitudinalSections = verticalSections;

		color2 = col2;
	}

	Color getColor(Vector pos);
};
#endif