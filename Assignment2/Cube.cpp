#include "Cube.h"
#include "Vector.h"
#include "Plane.h"

Vector Cube::transform(const glm::mat4x4 &t, Vector vec)
{
	glm::vec4 glVec = glm::vec4(vec.x, vec.y, vec.z, 1.0f);
	glVec = t * glVec;

	return Vector(glVec.x, glVec.y, glVec.z);
}

float Cube::intersect(Vector pos, Vector dir)
{
	Vector tpos = this->transform(invRot, pos - centre);
	Vector tdir = transform(invRot, dir);

	float intersect = -1;
	for (int i = 0; i < sides.size(); i++)
	{
		float thisIntersect = sides[i]->intersect(tpos, tdir);
		if (thisIntersect > 0 && (thisIntersect < intersect || intersect < 0.001))
		{
			intersect = thisIntersect;
		}
	}

	if (intersect < 0.001)
		return -1.0;

	Vector intersects = tpos + tdir * intersect;
	intersects = transform(rot, intersects);

	return intersect;
}

Vector Cube::normal(Vector pos)
{
	Vector tPos = transform(invRot, pos - centre);

	for (int i = 0; i < sides.size(); ++i)
	{
		if (sides[i]->isInside(tPos))
		{
			return transform(transRot, sides[i]->normal(tPos));
		}
	}

	return Vector(0, 1, 0);
}