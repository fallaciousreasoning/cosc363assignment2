#ifndef H_CONE
#define H_CONE
#include "Object.h"

class Cone : public Object
{
private:
	float baseRadius;
	float height;

	Vector position;
public:
	Cone(Vector position, float baseRadius, float height, Color col):
		position(position), baseRadius(baseRadius), height(height)
	{
		color = col;
	}

	float intersect(Vector start, Vector dir);
	Vector normal(Vector pos);
};

#endif