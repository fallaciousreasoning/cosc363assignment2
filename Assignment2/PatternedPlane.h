#include "Plane.h"

#define LENGTH_SECTIONS 10
#define WIDTH_SECTIONS 10

class PatternedPlane : public Plane
{
private:
	Color colorB;
public:
	Color getColor(Vector);
	PatternedPlane(Vector pA, Vector pB, Vector pC, Vector pD, Color color1, Color color2)
		:Plane(pA, pB, pC, pD, color1)
	{
		colorB = color2;
		
	}
};