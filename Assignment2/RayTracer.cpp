// ========================================================================
// COSC 363  Computer Graphics  Lab07
// A simple ray tracer
// ========================================================================

#include <iostream>
#include <cmath>
#include <vector>
#include "Vector.h"
#include "Sphere.h"
#include "Color.h"
#include "Object.h"
#include "Plane.h"
#include <GL/glut.h>
#include "PatternedPlane.h"
#include "Cube.h"
#include "TexturedSphere.h"
#include "PatternedSphere.h"
#include "Cylinder.h"
#include "Cone.h"

using namespace std;

const float WIDTH = 20.0;  
const float HEIGHT = 20.0;
const float EDIST = 40.0;
const int PPU = 30;     //Total 600x600 pixels
const int MAX_STEPS = 5;
const float XMIN = -WIDTH * 0.5;
const float XMAX =  WIDTH * 0.5;
const float YMIN = -HEIGHT * 0.5;
const float YMAX =  HEIGHT * 0.5;

vector<Object*> sceneObjects;

Vector light = Vector(10.0, 40.0, -5.0);
Color backgroundCol = Color::GRAY;

//A useful struct
struct PointBundle   
{
	Vector point;
	int index;
	float dist;
};

/*
* This function compares the given ray with all objects in the scene
* and computes the closest point  of intersection.
*/
PointBundle closestPt(Vector pos, Vector dir)
{
    Vector  point(0, 0, 0);
	float min = 10000.0;

	PointBundle out = {point, -1, 0.0};

    for(int i = 0;  i < sceneObjects.size();  i++)
	{
        float t = sceneObjects[i]->intersect(pos, dir);
		if(t > 0)        //Intersects the object
		{
			point = pos + dir*t;
			if(t < min)
			{
				out.point = point;
				out.index = i;
				out.dist = t;
				min = t;
			}
		}
	}

	return out;
}

/*
* Computes the colour value obtained by tracing a ray.
* If reflections and refractions are to be included, then secondary rays will 
* have to be traced from the point, by converting this method to a recursive
* procedure.
*/

Color trace(Vector pos, Vector dir, int step)
{
    PointBundle q = closestPt(pos, dir);


    if(q.index == -1) return backgroundCol;        //no intersection
	
	Object *sceneObject = sceneObjects[q.index];

	Vector n = sceneObject->normal(q.point);
	Vector l = light - q.point;
	l.normalise();

	double lDotn = l.dot(n);
	Vector r = ((n * 2) * lDotn) - l;
	r.normalise();

	Color col = sceneObjects[q.index]->getColor(q.point); //Object's colour
	Color colSum;

	PointBundle shadowPoint = closestPt(q.point, l);
	if (shadowPoint.index > -1  && shadowPoint.index != q.index){
		 colSum = col.phongLight(backgroundCol, 0.0, 0.0);
	}
	else {
		Vector v(-dir.x, -dir.y, -dir.z);
		float rDotv = r.dot(v);
		float spec;
		if (rDotv < 0.01) spec = 0.0;
		else spec = pow(rDotv, 15);
		colSum = col.phongLight(backgroundCol, lDotn, spec);
	}

	if (sceneObject->isReflective && step < MAX_STEPS && shadowPoint.index == -1)
	{
		Color reflectionColor = trace(q.point, r, step + 1);
		colSum.combineColor(reflectionColor, 0.65);
	}

	if (sceneObject->transparent && step < MAX_STEPS)
	{
		float transCoeff = 0.95f;

		float eta1 = 1.0f;
		float eta2 = 1.01f;
		float eta = eta1 / eta2;
		float dot = dir.dot(n);

		float cosThetaT = sqrt(1 - eta*eta * (1 - dot * dot));
		Vector innerRefrVector = dir*eta - n*(eta*dot + cosThetaT);

		PointBundle outer = closestPt(q.point, innerRefrVector);
		Vector outerRefrVector;
		if (outer.index != -1)
		{
			Object *outerObj = sceneObjects[outer.index];
			Vector normal = outerObj->normal(outer.point) * -1;

			float tmp = eta1;
			eta1 = eta2;
			eta2 = tmp;
			eta = eta1 / eta2;
			dot = innerRefrVector.dot(normal);
			cosThetaT = sqrt(1 - eta*eta * (1 - dot * dot));
			outerRefrVector = dir*eta - normal * (eta * dot + cosThetaT);
		} else
		{
			outerRefrVector = dir;
		}

		Color transparentColor = trace(outer.point, outerRefrVector, step + 1);
		colSum.combineColor(transparentColor, transCoeff);
	}
	return colSum;
}

//Gets the average color for a point 
Color AverageColor(Vector pos, float pixelSize, float x, float y)
{
	int sampleSize = 4; //It should be a power of two
	int rootSampleSize = (int)sqrtf(sampleSize);
	float scale = rootSampleSize / sampleSize;

	Vector direction;
	Color color;
	Color average = Color(0, 0, 0);

	for (int i = 0; i < rootSampleSize; ++i)
	{
		for (int j = 0; j < rootSampleSize; ++j)
		{
			float fragX = x + i * (scale*pixelSize);
			float fragY = y + j * (scale*pixelSize);

			direction = Vector(fragX, fragY, -EDIST);
			direction.normalise();

			color = trace(pos, direction, 1);

			average.r += color.r / sampleSize;
			average.g += color.g / sampleSize;
			average.b += color.b / sampleSize;
		}
	}

	return average;
}


//---The main display module -----------------------------------------------------------
// In a ray tracing application, it just displays the ray traced image by drawing
// each pixel as quads.
//---------------------------------------------------------------------------------------
void display()
{
	int widthInPixels = (int)(WIDTH * PPU);
	int heightInPixels = (int)(HEIGHT * PPU);
	float pixelSize = 1.0/PPU;
	float halfPixelSize = pixelSize/2.0;
	float x1, y1, xc, yc;
	Vector eye(0., 0., 0.);

	glClear(GL_COLOR_BUFFER_BIT);

	glBegin(GL_QUADS);  //Each pixel is a quad.

	for(int i = 0; i < widthInPixels; i++)	//Scan every "pixel"
	{
		x1 = XMIN + i*pixelSize;
		xc = x1 + halfPixelSize;
		for(int j = 0; j < heightInPixels; j++)
		{
			y1 = YMIN + j*pixelSize;
			yc = y1 + halfPixelSize;

		    Vector dir(xc, yc, -EDIST);	//direction of the primary ray

		    dir.normalise();			//Normalise this direction

			//Modify this line to turn antialiasing on or off
			Color col = AverageColor(eye, pixelSize, x1, y1);//trace (eye, dir, 1); //Trace the primary ray and get the colour value
			glColor3f(col.r, col.g, col.b);
			glVertex2f(x1, y1);				//Draw each pixel with its color value
			glVertex2f(x1 + pixelSize, y1);
			glVertex2f(x1 + pixelSize, y1 + pixelSize);
			glVertex2f(x1, y1 + pixelSize);
        }
    }

    glEnd();
    glFlush();
}



void initialize()
{
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(XMIN, XMAX, YMIN, YMAX);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glClearColor(0, 0, 0, 1);

	Sphere *sphere1 = new Sphere(Vector(3, 3, -70), 3.0, Color::RED);
	//sphere1->isReflective = true;
	sceneObjects.push_back(sphere1);

	Sphere *sphere2 = new Sphere(Vector(-2, 0, -50), 1.0, Color::BLUE);
	sphere2->transparent = true;
	sceneObjects.push_back(sphere2);

	Sphere *sphere3 = new PatternedSphere(Vector(10, -5, -70), 5.0, 16, 8, Color::RED, Color::GREEN);
	sphere3->transparent = true;
	sceneObjects.push_back(sphere3);

	Sphere *sphere4 = new TexturedSphere(Vector(-5, 0, -60), 4.0, "Earth.raw", 256, 128);
	sphere4->isReflective = true;
	sceneObjects.push_back(sphere4);

	Cube *cube1 = new Cube(Vector(0, -7.5, -50), 4, 5, 4, 1, Color::GREEN);
	sceneObjects.push_back(cube1);

	Cylinder *cylinder1 = new Cylinder(Vector(-7.5, -7.5, -50), 3, 5, Color::RED);
	sceneObjects.push_back(cylinder1);

	Plane *plane = new PatternedPlane(Vector(-10, -10, -40), Vector(10, -10, -40), Vector(10, -10, -80), Vector(-10, -10, -80), Color(1, 0, 1), Color(0, 1, 1));
	sceneObjects.push_back(plane);
}


int main(int argc, char *argv[]) 
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB );
    glutInitWindowSize(600, 600);
    glutInitWindowPosition(20, 20);
    glutCreateWindow("Raytracing");

    glutDisplayFunc(display);
    initialize();

    glutMainLoop();
    return 0;
}
