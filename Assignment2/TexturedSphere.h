#ifndef H_TSPHERE
#define H_TSPHERE

#include "Sphere.h"
#include "loadRaw.h"

class TexturedSphere : public Sphere{
protected:
	Color* texture;
	int _textureWidth, _textureHeight;
	
public:
	TexturedSphere(Vector centre, float radius, std::string filename, int textureWidth, int textureHeight)
		:Sphere(centre, radius, Color::WHITE)
	{
		texture = loadRAW(filename, textureWidth, textureHeight);
		_textureHeight = textureHeight;
		_textureWidth = textureWidth;

	}

	Color getColor(Vector pos);
};

#endif