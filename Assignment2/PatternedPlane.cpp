#include "PatternedPlane.h"
Color PatternedPlane::getColor(Vector pos)
{
	int posX = (pos.x - a.x) / ((a - b).x / WIDTH_SECTIONS);
	int posZ = (pos.z - a.z) / ((b - c).z / LENGTH_SECTIONS);

	if ((posX % 2 == 0 || posZ % 2 == 0) && (posX % 2 != posZ % 2))
	{
		return color;
	}
	return colorB;
}
