#include "PatternedSphere.h"
#include "math.h"

Color PatternedSphere::getColor(Vector pos)
{
	float PI = 3.14159264;
	int yCoord = static_cast<int>((0.5 + asinf((pos.y - center.y) / radius) / PI)*latitudinalSections);
	int xCoord = static_cast<int>((0.5 + atanf((pos.x - center.x) / (pos.z - center.z)) / PI)*longditudinalSections*0.5);

	if ((xCoord % 2 == 0 || yCoord % 2 == 0) && (xCoord % 2 != yCoord % 2))
	{
		return color;
	}
	return color2;
}
