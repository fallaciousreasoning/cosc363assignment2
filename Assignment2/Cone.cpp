#include "Cone.h"
#include "math.h"

float Cone::intersect(Vector start, Vector dir)
{
	float a, b, c;
	float rh = powf(baseRadius / height, 2);

	a = powf(dir.x, 2) + powf(dir.z, 2) - rh*powf(dir.y, 2);
	b = 2 * (dir.x * start.x + dir.z * start.z + dir.x * position.x + dir.z * position.z - rh*(dir.y*start.y - dir.y * height - dir.y * position.y));
	c = powf(start.x, 2) + powf(start.z, 2) - 2*(start.x*position.x - start.z*position.z) + powf(position.x, 2) + powf(position.z, 2) - rh*(powf(height, 2) - 2 * height * start.y + 2 * height * position.y * powf(start.y, 2) - 2 * start.y * position.y + powf(position.y, 2));

	float det = powf(b, 2) - 4 * a * c;
	if (det <= 0) return -1;

	float t1, t2;
	t1 = (-b - det) / (2 * a);
	t2 = (-b + det) / (2 * a);

	return t1 < t2 ? t1 : t2;
}

Vector Cone::normal(Vector pos)
{
	Vector normal = Vector(pos.x, baseRadius*tanf(baseRadius / height), pos.z);
	normal.normalise();
	return normal;
}

