#include "TexturedSphere.h"
#include "math.h"

Color TexturedSphere::getColor(Vector pos)
{
	float PI = 3.14159264;
	int yCoord = static_cast<int>((0.5 + asinf((pos.y - center.y) / radius) / PI)*_textureHeight);
	int xCoord = static_cast<int>((0.5 + atanf((pos.x - center.x) / (pos.z - center.z)) / PI)*_textureWidth*0.5);

	int index = xCoord + _textureWidth * yCoord;
	return texture[index];
}
