//======================================================================
// LoadRAW.h
// Image loader for files in RAW format.
// Assumption:  Colour image in RGB format (24 bpp) in interleaved order.
// Note: RAW format stores an image in top to bottom order, and will
//       appear vertically flipped.
// Author:
// R. Mukundan, Department of Computer Science and Software Engineering
// University of Canterbury, Christchurch, New Zealand.
//======================================================================
#ifndef H_RAW
#define H_RAW

#include <iostream>
#include <fstream>
#include "Color.h"
using namespace std;

inline Color* loadRAW(string filename, int width, int height)
{
    Color* colors;
	char* colorData;

    int size;
	int pixelSize;
    ifstream file( filename.c_str(), ios::in | ios::binary);
	if(!file)
	{
		cout << "*** Error opening image file: " << filename.c_str() << endl;
		exit(1);
	}

	pixelSize = width * height;  //Total number of pixels to be read
	size = pixelSize * 3; //Total number of bytes to be read
	colors = new Color[pixelSize];
	colorData = new char[size];

	file.read(colorData, size);

	for (int i = 0; i < pixelSize; ++i)
	{
		float r = (colorData[i * 3] & 0xFF) / 255.0;
		float g = (colorData[i * 3 + 1] & 0xFF) / 255.0;
		float b = (colorData[i * 3 + 2] & 0xFF) / 255.0;

		colors[i] = Color(r, g, b);
	}

	delete colorData;	
	return colors;
}


#endif

