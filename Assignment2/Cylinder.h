#ifndef H_CYLINDER
#define H_CYLINDER
#include "Object.h"

class Cylinder : public Object
{
private:
	Vector position;
	float height;
	float radius;
public:
	Cylinder(Vector position, float radius, float height, Color col):
		position(position), height(height), radius(radius)
	{
		color = col;
	}

	float intersect(Vector start, Vector dir);
	Vector normal(Vector pos);
};


#endif