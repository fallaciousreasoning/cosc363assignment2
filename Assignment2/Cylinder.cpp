#include "Cylinder.h"
#include "math.h"
#include <algorithm>

float Cylinder::intersect(Vector start, Vector dir)
{
	float a, b, c;

	a = (dir.x * dir.x) + (dir.z * dir.z);
	b = 2 * (dir.x* (start.x - position.x) + dir.z*(start.z - position.z));
	c = (start.x - position.x) * (start.x - position.x) + (start.z - position.z) * (start.z - position.z) - radius * radius;

	float det = b*b - 4 * a*c;
	float t1 = (-b - sqrt(det))/(2*a);
	float t2 = (-b + sqrt(det)) / (2 * a);

	if (fabs(det) < 0.001 || det < 1) return -1;

	float dist1 = fabs(start.y + dir.y * t1 - position.y);
	float dist2 = fabs(start.y + dir.y * t2 - position.y);

	float halfHeight = height * 0.5;

	if (dist1 < -halfHeight || dist1 > halfHeight)
	{
		if (dist2 < -halfHeight || dist2 > halfHeight)
		{
			return -1;
		}
		if (t2 > 0.001) return t2;
	}
	
	return t1 < t2 ? t1 : t2;
}

Vector Cylinder::normal(Vector pos)
{
	Vector normal = pos - position;
	normal.y = 0;
	normal.normalise();

	return normal;
}


